package by.agh.springbatchdemo;

import lombok.Data;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.builder.JdbcCursorItemReaderBuilder;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.batch.item.file.builder.FlatFileItemWriterBuilder;
import org.springframework.batch.item.file.transform.DelimitedLineAggregator;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import javax.sql.DataSource;
import java.io.File;
import java.util.Collections;
import java.util.Map;


@SpringBootApplication
public class SpringBatchDemoApplication {

    @Data
    public static class Man {
        private int age;
        private String firstName, email;
    }

    @Configuration
    public static class Step1Configuration
    {

        @Bean
        FlatFileItemReader<Man> fileReader(@Value("${input}") Resource in) {
            return new FlatFileItemReaderBuilder<Man>()
                    .resource(in)
                    .name("file-reader")
                    .targetType(Man.class)
                    .delimited().delimiter(",").names("firstName","age","email")
                    .build();
        }

        @Bean
        JdbcBatchItemWriter<Man> jdbcWriter(DataSource ds){
            return new JdbcBatchItemWriterBuilder<Man>()
                    .dataSource(ds)
                    .sql("insert into PEOPLE( AGE, FIRST_NAME, EMAIL) values (:age, :firstName, :email")
                    .beanMapped()
                    .build();
        }
    }

    @Configuration
    public static class Step2Configuration
    {
        @Bean
        ItemReader<Map<Integer, Integer>> jdbcReader(DataSource dataSource)
        {
            return new JdbcCursorItemReaderBuilder<Map<Integer, Integer>>()
                    .dataSource(dataSource)
                    .name("jdbc-reader")
                    .sql("select COUNT(age) c, age a from PEOPLE group by age")
                    .rowMapper((rs,i) -> Collections.singletonMap(
                            rs.getInt("a"),
                            rs.getInt("c")
                    ))
                    .build();
        }

        @Bean ItemWriter<Map<Integer, Integer>> fileWriter(@Value("${output}") Resource resource)
        {
            return new FlatFileItemWriterBuilder<Map<Integer, Integer>>()
                    .name("file-writer")
                    .resource(resource)
                    .lineAggregator(new DelimitedLineAggregator<Map<Integer, Integer>>(){
                        {
                            setDelimiter(",");
                            setFieldExtractor(map -> {
                               Map.Entry<Integer, Integer> next = map.entrySet().iterator().next();
                               return new Object[]{next.getKey(), next.getValue()};
                            });
                        }
                    })
                    .build();
        }
    }

    @Bean
    Job job(JobBuilderFactory jbf, StepBuilderFactory sbf,
            Step1Configuration step1Configuration,
            Step2Configuration step2Configuration) {
        Step step1 = sbf.get("file-db")
                .<Man, Man>chunk(100)
                .reader(step1Configuration.fileReader(null))
                .writer(step1Configuration.jdbcWriter(null))
                .build();

        Step step2 = sbf.get("db-file")
                .<Map<Integer,Integer>, Map<Integer, Integer>> chunk(1000)
                .reader(step2Configuration.jdbcReader(null))
                .writer(step2Configuration.fileWriter(null))
                .build();

        return jbf.get("etl")
                .incrementer(new RunIdIncrementer())
                .start(step1)
                .next(step2)
                .build();
    }

    public static void main(String[] args) {
        System.setProperty("input", "file://" + new File("/usr/user/Desktop/in.csv"));
        System.setProperty("output", "file://" + new File("/usr/user/Desktop/out.csv"));
        SpringApplication.run(SpringBatchDemoApplication.class, args);
    }

}
